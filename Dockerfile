FROM debian:9 as build
#update system and install software
RUN apt update && apt install -y git quilt debhelper libluajit-5.1-dev libluajit-5.1-2 unzip zlib1g-dev libpcre++-dev apt-utils wget gcc make
#install luajit2
RUN git clone https://github.com/openresty/luajit2 && cd luajit2/ && make && make install
#
RUN git clone https://github.com/simplresty/ngx_devel_kit
#
RUN git clone https://github.com/openresty/lua-nginx-module
#download and compile nginx
RUN export LUAJIT_LIB=/usr/local/lib && export LUAJIT_INC=/usr/local/include/luajit-2.1 && wget http://nginx.org/download/nginx-1.9.15.tar.gz && tar xvfz nginx-1.9.15.tar.gz && cd nginx-1.9.15 && ./configure --add-module=/ngx_devel_kit $
CMD ["/usr/local/nginx/sbin", " -g", "daemon off;"]

FROM debian:9
RUN apt update && apt install -y git
RUN git clone https://github.com/openresty/luajit2 && cd luajit2/ && make && make install
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
#COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/libluajit-5.1.so.2
RUN export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH && mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["/usr/local/nginx/sbin/nginx", " -g", "daemon off;"]